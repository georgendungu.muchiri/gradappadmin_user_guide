User Management 
===============

The super admin of the system is incharge of adding the other admins to the platform.The details to add include the following.

	#. Enter Name 
	#. Enter Email 
	#. Enter Password 
	#. Assign Role
	#. Assign Permissions
	
After adding an admin to the system they will receive an email notification to prompt them to activate their account so that they can have access to the system.

.. image:: usermanagement.png
	:align: center

