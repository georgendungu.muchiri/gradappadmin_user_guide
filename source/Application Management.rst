Application Management 
======================
An admin can be able to view all the applications made on this page and also on this page is where the admin is able to review the applicant details and confirm if an applicant is eligible to proceed with the application.To view applications go to applications on the side menu and click on the applications menu.The process of reviewing an application is as follows.

    #. Review Application Details 
    #. Review Documents 
    #. Enter application status(whether eligible or rejected) 
    #. Enter comment
    #. Confirm Eligibility

.. image:: Applications.PNG
	:align: center
