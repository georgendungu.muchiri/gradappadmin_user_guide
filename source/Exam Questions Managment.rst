Exam Questions Management
=========================
An admin can be able to add questions on the platform and assign those questions to an exam bank.The process involves creating exam questions and allocation marks to each question.

 .. image:: add_questions.png
	:align: center

    
The question can be edited by using the edit button

.. image:: edit_questions.png
   :align: center
 
If its a multiple choice question, the multiple choice options are added using the “ADD MULTICHOICE OPTIONS” button


