Exam Management
===============

An admin can be able to add questions on the platform and assign those questions to an exam bank.The process involves creating exam questions and allocation marks to each question.

.. image:: add_exams.png
	:align: center

At this point the created exam is made available for applicants to book
An admin can be able to create a schedule for the exam slots that the applicant needs to do before being admitted.The following details need to be submitted;
#. Enter exam type name 
#. Enter course 
#. Select Examiner 
#. Enter the exam date and time

.. image:: addExam.png
	:align: center
