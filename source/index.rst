.. image:: logo1.jpg
	:align: center


Welcome to GradApp Admin User Guide!
=====================================


GradApp platform allows an admin to perfrom all neccesary adminstrative functions for management of masters application.


Contents:
=========

.. toctree::
   :maxdepth: 1
   
   Getting Started
   Login Process 
   Roles and Permission Management
   User Management 
   Application Management 
   Interview Schedule Management
   Exam Management 
   Exam Questions Management
   Exam Questions Selection
   Mark Exams



Links:
======

* **Admin Portal**
	* `GradApp <http://gradappadmin.strathmore.edu/>`_
