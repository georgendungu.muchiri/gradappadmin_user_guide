Interview Schedule management
=============================

An admin can be able to create a schedule for an oral interview slot by doing the following;
Enter the date and time for an interview
Create a meeting link (using zoom or google meet)

.. image:: interviewschedules.png
	:align: center
