Roles and Permission Management
===============================
An admin needs to first add the different user roles of the various admins that will be interacting with the admin platform and also grant them the different permissions each user requires.To add the roles you navigate on the dashboard under access control.

.. image:: Roles.png
	:align: center