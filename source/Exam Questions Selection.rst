Exam Questions Management
=========================

Select Questions for Exam
 On the Exam list go to 

 .. image:: Select-Questions.png
	:align: center


Questions relevant to the course will be listed, click the “+ADD” button to add the question, or “x REMOVE” to remove the question.
