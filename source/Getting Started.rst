Getting Started
===============

To get started as an Admin go  to http://gradappadmin.strathmore.edu/

Login Process 
=============

An admin needs to login to have access to the admin dashboard by entering the correct email and password to get access to the dashboard.

.. image:: Login-Page.png
	:align: center